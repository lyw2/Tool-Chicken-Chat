'''
@Author: hua
@Date: 2019-05-30 10:41:29
@LastEditors: hua
@LastEditTime: 2019-07-08 10:44:40
'''
from flask import jsonify, make_response
from app.env import DEBUG_LOG
from app.Vendor.Log import log
from app.Vendor.Utils import Utils
import traceback,sys

def ExceptionApi(code, e):
    """ 接口异常处理 """
    exc_type, exc_value, exc_traceback = sys.exc_info()
    if(DEBUG_LOG):
        log().exception(e)
    body = {}
    body['error_code'] = code
    body['error'] = True
    body['show'] = False
    body['debug_id'] = Utils.unique_id()
    #这里exc_type 和exc_value信息重复，所以不打印
    body['traceback'] = traceback.format_exception([], exc_value, exc_traceback)
    return make_response(jsonify(body))