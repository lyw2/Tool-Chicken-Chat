'''
@Author: hua
@Date: 2019-05-28 20:11:04
@LastEditors: hua
@LastEditTime: 2019-06-11 20:40:59
'''

validation = {
    #验证规则
    'required':{
        True:'必须',
        False: '非必须'
    },
    'type':'类型',
    'string': '字符串',
    'integer': '整形',
    'list': '列表',
    'minlength': '最小长度',
    'maxlength': '最大长度',
    #验证字段
    'nickName': '昵称',
    'headImg': '头像',
    'email': '邮箱',
    'password': '密码',
    'focused_user_id': '添加者编号',
    'be_focused_user_id': '被添加者',
    'page_no': '当前页',
    'per_page': '每页',
    'keywords': '关键字',
    'ids':'编号集合',
    'name': '用户名',
    'pwd': '密码',
    'captcha': '图形验证码',
    'uuid': '唯一编号',
    'keyword': '关键字',
    'id':'编号',
    'room_uuid':'房间编号'
}